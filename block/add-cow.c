#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>

#include "block/add-cow.h"
#include "qemu-common.h"
#include "block_int.h"

int get_add_cow_head(const char * add_cow_file, struct add_cow_head* p)
{
    int cow_fd = open(add_cow_file, O_RDONLY | O_BINARY,
               0644);
    if (cow_fd < 0)
        return -errno;
    
    memset(p, 0, sizeof(struct add_cow_head));

    if(read(cow_fd,p,sizeof(struct add_cow_head)) == -1)
        return -errno;
    
    return 0;
}
 
int create_cow_file(const char *backing_file,const char *backing_format, const char *cow_file, uint64_t size)
{
    FILE* f;
    int ret = -1;
    struct add_cow_head header;
    
    uint64_t bitmap_size = size/8;
    if(!cow_file || !backing_file || !size)
    return 0;
    
    char* buf = qemu_mallocz(bitmap_size);
    
    memset(&header,0,sizeof(header));
    sprintf(header.backing_file,"%s",backing_file);
    sprintf(header.backing_format,"%s",backing_format?backing_format:"");
    header.size = size;

    memset(buf,0,1024);
    
    f = fopen(cow_file,"wb");
    if (!f) {
        return -errno;
    }

    if(fwrite((void *)&header,1,sizeof(header),f) != sizeof(header)) {
        return -errno;
    }
    
    ret = fseek(f,0,SEEK_END);
    if((ret = fwrite(buf,1,bitmap_size,f)) != bitmap_size){
        return -errno;
    }
    ret    = 0;

    fclose(f);
    return ret;
}

inline int my_cow_set_bit(BlockDriverState *bs, int64_t bitnum)
{
    uint64_t offset = sizeof(struct add_cow_head) + bitnum / 8;
    uint8_t bitmap;
    int ret;

    int cow_fd = open(bs->cow_file, O_RDWR | O_BINARY,
               0644);
    if (cow_fd < 0)
        return -errno;
    
    ret = pread(cow_fd,&bitmap,sizeof(bitmap),offset);
    if(ret<0)
        return ret;

    bitmap |= (1 << (bitnum % 8));

    ret = pwrite(cow_fd,&bitmap,sizeof(bitmap),offset);
    if( ret < 0 )
        return ret;
    
    return 0;
}

inline int my_is_bit_set(BlockDriverState *bs, int64_t bitnum)
{
    uint64_t offset = sizeof(struct add_cow_head) + bitnum / 8;
    uint8_t bitmap;
 
    int cow_fd = open(bs->cow_file, O_RDONLY | O_BINARY,
               0644);
    if (cow_fd < 0) {
        close(cow_fd);
        return -errno;
    }
    
    if(pread(cow_fd,&bitmap,sizeof(bitmap),offset) == -1) {
        close(cow_fd);
        return -errno;
    }
    close(cow_fd);
    return !!(bitmap & (1 << (bitnum % 8)));
}

int my_cow_is_allocated(BlockDriverState *bs, int64_t sector_num,
        int nb_sectors, int *num_same)
{
    int changed;

    if (nb_sectors == 0) {
        *num_same = nb_sectors;
        return 0;
    }

    changed = my_is_bit_set(bs, sector_num);
    if (changed < 0) {
        return 0;
    }

    for (*num_same = 1; *num_same < nb_sectors; (*num_same)++) {
        if (my_is_bit_set(bs, sector_num + *num_same) != changed)
            break;
    }

    return changed;
}

int my_cow_update_bitmap(BlockDriverState *bs, int64_t sector_num,
        int nb_sectors)
{
    int error = 0;
    int i;

    for (i = 0; i < nb_sectors; i++) {
        error = my_cow_set_bit(bs, sector_num + i);
        if (error) {
            break;
        }
    }

    return error;
}

