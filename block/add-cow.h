#ifndef QEMU_ADD_COW_H
#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdio.h>
#include <errno.h>

#include "qemu-common.h"

struct add_cow_head {
    char backing_file[1024];
    char backing_format[1024];
    uint64_t size;
};

int get_add_cow_head(const char * add_cow_file, struct add_cow_head* p);
int create_cow_file(const char *backing_file,const char *backing_format, const char *cow_file, uint64_t size);
int my_cow_update_bitmap(BlockDriverState *bs, int64_t sector_num,int nb_sectors);
int my_cow_is_allocated(BlockDriverState *bs, int64_t sector_num, int nb_sectors, int *num_same);
inline int my_is_bit_set(BlockDriverState *bs, int64_t bitnum);
inline int my_cow_set_bit(BlockDriverState *bs, int64_t bitnum);

#endif

